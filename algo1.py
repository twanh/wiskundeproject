import numpy as np
from random import randint
# Create a basic np matrix
# The matrix has to be 6x6, as specified in the math project

coins = 6

# The size that the array/matrix
size = (coins,coins)
# The actual array (filled with zeros.)

def algo1():
    # Create new empty matrix with the correct size, and datatype
    temp = np.zeros(size, dtype=int)

    # Set a starting value
    temp[randint(0, 5)][randint(0, 5)]=1

    # Keep track of the amount of coins used
    coins_used = 0
    # Enumerate through the matrix
    for rowIndx, row in enumerate(temp):    
        # Print for debugging purposes  
        print("[{}]-{}".format(rowIndx, row))

        # Enumerate through the row     
        for nIndx, n in enumerate(row):

            # Get the neighbours (can later be used to check if there is a neighbour with a 1 or 0)
            top = temp[rowIndx-1][nIndx] if rowIndx!=0 else 0
            left = row[nIndx-1] if nIndx!=0 else 0
            right = row[nIndx+1] if nIndx!=(len(row)-1) else 0

            # If a neighbour is 1, be one aswell         
            if top == 1 or left == 1 or right == 1:
                if coins_used >= coins:
                    pass
                else:
                    temp[rowIndx][nIndx] = 1
                    coins_used += 1



            # Print for debugging purposes  
            print("\t[{}]-|n:{}|t:{}|l:{}|r:{}|".format(nIndx, n, top, left, right))
    return temp

print(algo1())
