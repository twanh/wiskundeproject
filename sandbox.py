import numpy as np
from random import randint

# The amount of coins you have
# It defines the matrix and the restrictions
coins = 6

# The size that the array/matrix
size = (coins,coins)


class Pattern:
    def __init__(self, coins, debug=False):
        self.COINS = coins
        self.DEBUG = debug

    def createEmptyMatrix(self):
        '''
        Creates an empty matrix (actually a 2d array)
        return: np.array 
        '''
        return np.zeros((self.COINS, self.COINS), dtype=int)

    def createPattern(self, startingX, startingY):
        # Create the 2dArray
        matrix = self.createEmptyMatrix()
        # Set a starting point for the pattern
        # TODO: have input checking and type valdidation
        matrix[startingX][startingY]=1

        # Set a coin counter
        coins_used = 0

        # Enumerate through the matrix
        for rowIndx, row in enumerate(matrix):
            if self.DEBUG: print("[{}]-{}".format(rowIndx, row))
            
            # Enumerate through the row
            for nIndx, n in enumerate(row):

                # Find the neighbours
                # Note: Could be own function
                top = matrix[rowIndx-1][nIndx] if rowIndx != 0 else 0
                left = row[nIndx-1] if nIndx != 0 else 0
                right = row[nIndx+1] if nIndx != (len(row)-1) else 0

                # Check if there is a neighbour with 0
                if top == 1 or left == 1 or right == 1:
                    # Check if there are still coins left to use
                    if coins_used >= coins:
                        pass
                    else:
                        # If there are still coins left -> fill the current spot
                        matrix[rowIndx][nIndx]=1
                        coins_used += 1
                

                if self.DEBUG: print("\t[{}]-|n:{}|t:{}|l:{}|r:{}|".format(nIndx, n, top, left, right))

        return matrix


# def createPattern():
#     # Create new empty matrix with the correct size, and datatype
#     temp = np.zeros(size, dtype=int)

#     # Set a starting value
#     temp[randint(0, 5)][randint(0, 5)]=1

#     # Keep track of the amount of coins used
#     coins_used = 0
#     # Enumerate through the matrix
#     for rowIndx, row in enumerate(temp):    
#         # Print for debugging purposes  
#         print("[{}]-{}".format(rowIndx, row))

#         # Enumerate through the row     
#         for nIndx, n in enumerate(row):

#             # Get the neighbours (can later be used to check if there is a neighbour with a 1 or 0)
#             top = temp[rowIndx-1][nIndx] if rowIndx!=0 else 0
#             left = row[nIndx-1] if nIndx!=0 else 0
#             right = row[nIndx+1] if nIndx!=(len(row)-1) else 0

#             # If a neighbour is 1, be one aswell         
#             if top == 1 or left == 1 or right == 1:
#                 if coins_used >= coins:
#                     pass
#                 else:
#                     temp[rowIndx][nIndx] = 1
#                     coins_used += 1



#             # Print for debugging purposes  
#             print("\t[{}]-|n:{}|t:{}|l:{}|r:{}|".format(nIndx, n, top, left, right))
#     return temp

# print(createPattern())

Pattern1 = Pattern(6, True).createPattern(2, 5)
print(Pattern1)