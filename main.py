import numpy as np

# Set the constant/config variables
COINS = 6
SIZE = (COINS, COINS)

def createEmptyMatrix(size=(SIZE)):
    '''
    Create an empty matrix(/2D array) based on the inputed size

    Args:
        size (tupal, optional): the size of the matrix
    
    Returns:
        np.array: An empty matrix (filled with 0s)
    '''
    return np.zeros(size, dtype=int)


def findNeighbours(matrix, curCoordinates):
    '''
    Find the neighbours of a point in a matrix,
     the point is where curCoordinates is at.
    
    Args:
        matrix (2d np.array): The matrix to find the neighbours in
        curCoordinates (tuple): The current coordinates/the current point formatted as (x, y)

    Returns:
        tuple(top, left, right): the values of the neighbours

    '''
    x, y = curCoordinates
    top = matrix[y-1][x] if y != 0 else 0
    left = matrix[y][x-1] if x != 0 else 0
    right = matrix[y][x+1] if x != (len(matrix[y])-1) else 0

    return (top, left, right)



def createPattern(startX, startY):
    '''
    Create a pattern based on an empty matrix. 
    The starting point of the pattern will be startX, startY which will
     corespond with matrix[y][x]

    Args:
        startX (int): The starting x point of the pattern
        startY (int): The starting y point of the pattern

    Returns:
        2d np.array: The matrix create with the pattern
    '''
    
    # Create a matrix/2d array
    matrix = createEmptyMatrix()
    # Set the starting points
    matrix[startY][startX]=1

    coinPoints = []

    # Create and set a coin counter
    coinsUsed = 0
    # Enumerate through the matrix, allowing for the pattern to be created
    for rowIndx, row in enumerate(matrix):
    
        # Enumerate through the row1
        for nIndx, n in enumerate(row):
            # Check if there is a neighbour with a 1
            if 1 in findNeighbours(matrix, (nIndx, rowIndx)):
                # Check if there are still coins left to coins_used
                if coinsUsed >= COINS:
                    pass
                else:
                    matrix[rowIndx][nIndx]=1
                    coinsUsed += 1
                    coinPoints.append((nIndx, rowIndx))

            # Check if all the coins are used
            if rowIndx == len(matrix)-1 and coinsUsed < COINS and nIndx == len(row)-1:
                # Calculate the remaining coins
                remainingCoins = COINS - coinsUsed
                # Find possible locations
                    # print(freeCoordinates)

                # Loop trough the remaining coins
                for coin in range(remainingCoins):
                    for coinPoint in coinPoints:
                        # Check if there is still a possible location as neighbour of this coin
                        neighbours = findNeighbours(matrix, coinPoint)
                        freeCoordinates = []
                        if 0 in neighbours:
                            # Check which neighbour is availible
                            if neighbours[0] == 0:
                                # Top is availible
                                freeCoordinates.append((nIndx, rowIndx+1))
                            if neighbours[1] == 0:
                                # Left is availible
                                freeCoordinates.append((nIndx-1, rowIndx))
                            if neighbours[2] == 0:
                                # Right is availible
                                freeCoordinates.append((nIndx+1, rowIndx))    
                    
                          


    return matrix
